async function getWeatherData() {
  try {
    const input = document.querySelector("input");
    const key = "1d0fc18c89117f1a01db99760710bb9e";
    const url = "https://api.openweathermap.org";
    const endpoint = `${url}/data/2.5/weather?q=${input.value}&units=metric&APPID=${key}`;
    const response = await fetch(`${endpoint}`);
    const data = await response.json();
    console.log(data);
    if (data.cod !== 200) {
      const message = data.message;
      document.querySelector(".not_found").style.display = "block";
      document.querySelector(".not_found").textContent = message;
    } else {
      document.querySelector(".not_found").style.display = "none";
      const weather = data.weather[0].description;
      const city = data.name;
      const icon = data.weather[0].icon;
      const temp = data.main.temp;
      const feelsLike = data.main.feels_like;
      const minTemp = data.main.temp_min;
      const maxTemp = data.main.temp_max;

      document.querySelector(".city").textContent = city;
      document.querySelector(".temperature").innerHTML = `${temp}<sup>o</sup>C`;
      document.querySelector(".icon").src = iconUrl;
      document.querySelector(".description").textContent = weather;
      document.querySelector(
        ".feels_like"
      ).innerHTML = `Feels Like: ${feelsLike} <sup>o</sup>C`;

      document.querySelector(
        ".min_temp"
      ).innerHTML = `min-temp: ${minTemp}<sup>o</sup>C`;
      document.querySelector(
        ".max_temp"
      ).innerHTML = `max-temp: ${maxTemp}<sup>o</sup>C`;
    }
  } catch (error) {
    console.log(error);
    document.querySelector(
      ".not_found"
    ).textContent = `Error fetching data! ERR: ${error}`;
  }
}

document.querySelector("button").addEventListener("click", () => {
  getWeatherData();
  document.querySelector(".weather_card").style.display = "block";
});
